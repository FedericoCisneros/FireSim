﻿namespace FireSim
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.comenzar = new System.Windows.Forms.Button();
            this.cargarArchivo = new System.Windows.Forms.Button();
            this.configuracion = new System.Windows.Forms.Button();
            this.archivo = new System.Windows.Forms.OpenFileDialog();
            this.imgSimulador = new System.Windows.Forms.PictureBox();
            this.logoHeap = new System.Windows.Forms.PictureBox();
            this.simulador = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgSimulador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoHeap)).BeginInit();
            this.SuspendLayout();
            // 
            // comenzar
            // 
            this.comenzar.Enabled = false;
            this.comenzar.Location = new System.Drawing.Point(281, 328);
            this.comenzar.Name = "comenzar";
            this.comenzar.Size = new System.Drawing.Size(200, 52);
            this.comenzar.TabIndex = 0;
            this.comenzar.Text = "Comenzar";
            this.comenzar.UseVisualStyleBackColor = true;
            this.comenzar.Click += new System.EventHandler(this.comenzar_Click);
            // 
            // cargarArchivo
            // 
            this.cargarArchivo.Location = new System.Drawing.Point(281, 299);
            this.cargarArchivo.Name = "cargarArchivo";
            this.cargarArchivo.Size = new System.Drawing.Size(102, 23);
            this.cargarArchivo.TabIndex = 1;
            this.cargarArchivo.Text = "Cargar Archivo";
            this.cargarArchivo.UseVisualStyleBackColor = true;
            this.cargarArchivo.Click += new System.EventHandler(this.cargarArchivo_Click);
            // 
            // configuracion
            // 
            this.configuracion.Location = new System.Drawing.Point(389, 299);
            this.configuracion.Name = "configuracion";
            this.configuracion.Size = new System.Drawing.Size(92, 23);
            this.configuracion.TabIndex = 2;
            this.configuracion.Text = "Configuracion";
            this.configuracion.UseVisualStyleBackColor = true;
            // 
            // archivo
            // 
            this.archivo.FileName = "archivo";
            // 
            // imgSimulador
            // 
            this.imgSimulador.Location = new System.Drawing.Point(245, 73);
            this.imgSimulador.Name = "imgSimulador";
            this.imgSimulador.Size = new System.Drawing.Size(287, 145);
            this.imgSimulador.TabIndex = 3;
            this.imgSimulador.TabStop = false;
            // 
            // logoHeap
            // 
            this.logoHeap.Image = global::FireSim.Properties.Resources.logoHEAP;
            this.logoHeap.Location = new System.Drawing.Point(245, 39);
            this.logoHeap.Name = "logoHeap";
            this.logoHeap.Size = new System.Drawing.Size(28, 28);
            this.logoHeap.TabIndex = 4;
            this.logoHeap.TabStop = false;
            // 
            // simulador
            // 
            this.simulador.AutoSize = true;
            this.simulador.Location = new System.Drawing.Point(279, 54);
            this.simulador.Name = "simulador";
            this.simulador.Size = new System.Drawing.Size(53, 13);
            this.simulador.TabIndex = 5;
            this.simulador.Text = "Simulador";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.simulador);
            this.Controls.Add(this.logoHeap);
            this.Controls.Add(this.imgSimulador);
            this.Controls.Add(this.configuracion);
            this.Controls.Add(this.cargarArchivo);
            this.Controls.Add(this.comenzar);
            this.Name = "Principal";
            this.Text = "FireSim";
            ((System.ComponentModel.ISupportInitialize)(this.imgSimulador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoHeap)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button comenzar;
        private System.Windows.Forms.Button cargarArchivo;
        private System.Windows.Forms.Button configuracion;
        private System.Windows.Forms.OpenFileDialog archivo;
        private System.Windows.Forms.PictureBox imgSimulador;
        private System.Windows.Forms.PictureBox logoHeap;
        private System.Windows.Forms.Label simulador;
    }
}

